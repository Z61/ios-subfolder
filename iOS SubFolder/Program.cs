﻿using System;
using System.Collections.Generic;
using System.IO;

namespace com.zeesixtyone.ios.subfolder
{
    class Program
    {
        private string directory = string.Empty;
         
        static void Main(string[] args)
        {
            string directory = string.Empty;
            FileInfo[] files = { };
#if DEBUG
            args = new[] { @"F:\ios" };
#endif
            if (args.Length == 0)
            {
                directory = getUserDirectory();
            }
            else
            {
                directory = args[0];
                if (Directory.Exists(directory))
                {
                    files = getFilesInfo(Directory.GetFiles(directory));
                }
            }
            Console.WriteLine($"Using Directory '{directory}'");
            if (files.Length > 0)
            {
                foreach (FileInfo file in files)
                {
                    createAndMove(file, ref directory);
                }
                Console.WriteLine("Process seems to have completed, gl;hf");
            }
            else
            {
                Console.WriteLine("There doesn't seem to be any files in that directory. Try a different directory.");
            }

            Console.Read();
        }
        static string getUserDirectory()
        {
            string directory = string.Empty;

            while (true)
            {
                Console.WriteLine("Please give the correct directory you wish to use.");
                string input = Console.ReadLine();
                if (Directory.Exists(input))
                {
                    directory = input;
                    break;
                }
                else
                {
                    Console.WriteLine("The directory you entered does not exist. Try again.");
                }
            }

            return directory;
        }

        static FileInfo[] getFilesInfo(string[] files)
        {
            List<FileInfo> info = new List<FileInfo>();

            foreach (string file in files)
            {
                if (File.Exists(file))
                {
                    info.Add(new FileInfo(file));
                }
            }

            return info.ToArray();
        }

        static void createAndMove(FileInfo file, ref string baseDirectory)
        {
            string directoryName = file.Name.Substring(0, 2);
            try
            {
                if (!Directory.Exists($"{baseDirectory}/{directoryName}"))
                {
                    Directory.CreateDirectory($"{baseDirectory}/{directoryName}");
                }
                file.MoveTo($"{baseDirectory}/{directoryName}/{file.Name}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An Error Occured while trying to move file {file.FullName}");
            }
            Console.WriteLine(file.Name);
        }
    }
}
